<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="stylesheet" type="text/css">
<link rel="icon" href="../ump.png" type="image/x-icon" />
<script src="modif_myscript.js"></script>
<style>

input[type="date"]
{
  margin-right:88px;
  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
input[type="time"]
{
  margin-right:88px;
  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}

select.a
{
  margin-right:88px;
  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}


  div.copyright
{
	text-align:right;
	margin-top:190px;
}
h4
{
	
	font-family:tahoma;
}
img.centrale
{
display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}
table.media
{
	float:left;
	margin-top:170px;
	border-spacing:20px;
}
table#boutons
{
    margin-left:auto;
	margin-right:auto;
}
input[type="submit"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
input[type="button"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
.postionate
{
	text-align:left;
	float:left;
}
pre
{
	font-size:12px;
}
table.inscription
{
	margin-left:auto;
	margin-right:auto;
}

img.c
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}


input[type="submit"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
input[type="button"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
div > div.notice
{
	text-align:center;
	font-family:arial;
	color:rgb(227, 230, 228);
}


div.bleu
{
	width:100%;
	height:220px;
	background-color:rgb(165, 194, 237);
	color:white;
	font-family:tahoma;
	font-size:15px;
	text-align:center;
	padding:20px 0px;
	font-weight:bold;
}
div.position
{
	margin-right:400px;
}
p.notice
{
color:red;	
}

</style>
<title>École Supérieure de Technologie</title>
<script>
function checking() {
  var filiere=document.getElementById('filiere').value;
  var annee=document.getElementById('annee').value;
  var startDate = new Date(document.getElementById('date').value);
  var today = new Date();
  if (startDate.getTime() < today.getTime()) {
    alert("La date que vous avez saisissez est non valide");
	event.preventDefault()
	  return false;
  }
  else if(filiere.includes('DUT') && annee.includes('LP'))
  {
	alert("La filiere ne correspond pas à l'année universitaire");
	event.preventDefault();
	return false;
  }
  else if((filiere.includes('LP') || filiere.includes('Licences')) && (annee.includes('1ére') || annee.includes('2éme')))
  {
	alert("La filiere ne correspond pas à l'année universitaire");
	event.preventDefault();
	return false;
  }
  else
  {
	  return true;
  }

}
</script>
</head>
<body onload="myfunction1()">
<img class="c" src="../ump.png">

<form name="formulaire" method="post" onsubmit="checking();" id="form" enctype="multipart/form-data">


<div class="bleu">
<table class="inscription">
<tr>
<td align="right">
<label for="filiere" class="postionate">Filière</label>
</td>
<td align="left">

<select name="filiere" id="filiere" class="a" >
<optgroup label="Choisissez votre filière">
<?php
$con=mysqli_connect("localhost","id13248667_root","","gestionds");
if($con)
{ 
	$result=mysqli_query($con,'select * from filiere');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
		{  
			echo "<option value='".$row["libelle_filiere"]."'>".$row["libelle_filiere"]."</option>";
		}
	}
}
mysqli_close($con);
?>
</optgroup>
</select><br>

</td>
</tr>
<tr>
<td>
<label for="annee" class="postionate">Année universitaire</label>
</td>
<td align="left">
<select name="annee" id="annee" class="a" >
<optgroup label="Choisissez l'année étudiantelle">
<?php
$con=mysqli_connect("localhost","id13248667_root","","gestionds");
if($con)
{
	$result=mysqli_query($con,'select * from annee');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
			echo "<option value='".$row["libelle_annee"]."'>".$row["libelle_annee"]."</option>";
	}
}
mysqli_close($con);
?>
</optgroup>
</select>
</td>
</tr>
<tr>
<td>
<label for="type" class="postionate">Type d'examen</label>
</td>
<td align="left">

<select name="type" id="type" class="a">
<optgroup label="Choisissez le type d'examen">
<option value="DS" required>DS</option>
<option value="TP">TP</option>
</optgroup>
</select>
</td>
</tr>
<tr>
<td>
<label for="date"  class="postionate"  >Date d'examen</label>
</td>
<td align="left">
<input type="date" style='width:168px' name="date" id="date" max="2020-12-31" min="2019-01-01" required><br>
</td>
</tr>
<tr>
<td>
<label for="date" class="postionate">Temps d'examen</label>
</td>
<td align="left">
<input type="time" style='width:168px' name="temps" id="temps" min="08:30" max="18:00" required><br>
</td>
</tr>
</table>
</div>

<table class="inscription" >
<tr>
<td>
<input type="button" name="acceuil" value="Accueil" onclick="history.replaceState(null,null,'exam_index.php');location.reload();">
</td>
<td>
<input type="submit" name="continuer" value="Continuer">
</td>
</form>

</tr>
</table>
<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>