<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:connexion_index_erreur.php");
exit;
}
?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">

<link rel="stylesheet" type="text/css" href="modifier_style.css" >
<link rel="icon" href="../ump.png" type="image/x-icon" />
<title>École Supérieure de Technologie</title>
</head>
<script>
function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}
function myfunction1()
{
 document.getElementById('old_libelle').value=get('libelle_matiere');
}
</script>
<body onload="myfunction1()">
<form name="formulaire" method="post" enctype="multipart/form-data">

<img  class="logo" src="../ump.png">

<div class="bleu">
<table class="content">
<tr>
<td>
<label for="old_libelle" class="postionate">Libellé d'année étudiantelle*</label>
</td>
<td>
<input type="text" size="50px" value=""  name="old_libelle" id="old_libelle" readonly><br>
</td>
</tr>
<tr>
<td>
<label for="new_libelle" class="postionate">Nouveau libellé*</label>
</td>
<td>
<input type="text" size="50px" placeholder="Saisissez un nouveau libellé" name="new_libelle" id="new_libelle"><br>
</td>
</tr>
</table>

</div>
<table class="content" >
<tr>
<td>
<input type="submit" name="submit" value="Valider">
</form>
</td>
<td>
<input  type="button" onclick="javascript:window.location.replace('matiere_index1.php')" value="Retourner">
</td>
</tr>
</table>
<?php
global $id_matiere;
global $id_filiere;
global $id_annee;
global $id_admin;
$id_matiere=$_REQUEST['id_matiere'];
$id_annee=$_REQUEST['id_annee'];
$id_filiere=$_REQUEST['id_filiere'];
$id_admin=$_REQUEST['id_admin'];
include 'modifier_matiere.php'; 
?>
<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="../youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="../facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="../esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>