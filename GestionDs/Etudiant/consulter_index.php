<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="stylesheet" type="text/css"  >
<style>
div.copyright
{
	text-align:right;
	margin-top:240px
}
table.media
{
	float:left;
	margin-top:220px;
	border-spacing:20px;
}
img.centrale
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}
input[type="image"]
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:400px;
}
input[type="button"]
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
  table {
                   width: 100%;
                   margin-top:1%;
                   border-width:5px;  
                   border-style:inset;
                   border-color:cornflowerblue;
              }
        th, td {
            padding: 5px;
  text-align: left;
    border-bottom: 1px solid #ddd;
    text-align:center;
}
        tr:hover {background-color :lightsteelblue}
        tr:nth-child(even) {background-color: #f2f2f2;}
         .button {
            background-color :dodgerblue;
            border: none;
            color: white;
            padding: 15px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size:20px;
            margin: 10px 1px;
            position:relative;
            top: 40px;
            left: 600px;
            right: 4px;
          
            font-family: 'Hind Vadodara',sans-serif;
       
        }
input[type="button"]:hover
{
  display: block;
  background-color:rgb(57,121,218);
  color:white;
  margin-left: auto;
  margin-right: auto;
  text-align:center;
}
div.bleu
{
	width:100%;
	height:180px;
	background-color:rgb(57,121,218);
		color:white;
	font-family:tahoma;
	font-size:15px;
	text-align:center;
	padding:20px 0px;
	font-weight:bold;
}
div.position
{
	margin-right:400px;
}

</style>
<link rel="icon" href="ump.png" type="image/x-icon" />
<title>École Supérieure de Technologie</title>
</head>
<body>

<img class="centrale" src="ump.png">
<br>
<?php 
function tempsFin($time1, $time2) {
      $time1 = date('H:i:s',strtotime($time1));
      $time2 = date('H:i:s',strtotime($time2));
      $times = array($time1, $time2);
      $seconds = 0;
      foreach ($times as $time)
      {
        list($hour,$minute,$second) = explode(':', $time);
        $seconds += $hour*3600;
        $seconds += $minute*60;
        $seconds += $second;
      }
      $hours = floor($seconds/3600);
      $seconds -= $hours*3600;
      $minutes  = floor($seconds/60);
      $seconds -= $minutes*60;
      if($seconds < 9)
      {
      $seconds = "0".$seconds;
      }
      if($minutes < 9)
      {
      $minutes = "0".$minutes;
      }
        if($hours < 9)
      {
      $hours = "0".$hours;
      }
      return "{$hours}:{$minutes}:{$seconds}";
    }
if(isset($_POST['chercher']))
{  
	if(!empty($_POST["filiere"]) && !empty($_POST["annee"]))
	{  
		 $con=mysqli_connect("127.0.0.1","root","","gestionds");
					 if($con)
					 {     
							$filiere=$_POST["filiere"];
							$annee=$_POST["annee"];
							echo" <h4 style='color:rgb(165, 194, 237);font-family:tahoma;'>$filiere</h4>";
							$result=mysqli_query($con,"select * from ajoutds a, professeur b where date>CURRENT_DATE and a.id_prof=b.id_prof and a.filiere='$filiere' and a.libelle_annee='$annee'");	
							 
							 
							  echo 
								"<table w ;>
								<tr>
								<th '>Matière</th>
								<th '>Année</th>
								<th '>Type d'examen</th>
								<th '>Salle</th>
								<th '>Date</th>
								<th '>Temps</th>
								<th '>Professeur</th>
								</tr>
								";
								if(mysqli_num_rows($result)>0)
								{
							while($row = mysqli_fetch_array($result))
							{
								$debut=date('H:i',strtotime($row['temps']));
								$fin=date('H:i',strtotime(tempsFin($row['temps'],$row['duree'])));
								if($row['sexe']=='Homme') $sexe="M.";
								if($row['sexe']=='Femme') $sexe="Mme.";
								
									echo 
									"<tr>
								    <td align='center' '>".$row['matiere']."</td>
									<td align='center' '>".$row['libelle_annee']."</td>
									<td align='center' '>".$row['type']."</td>
									<td align='center' '>".$row['salle']."</td>
									<td align='center' '>".$row['date']."</td>
									<td align='center' '>".$debut." à ".$fin."</td>
									<td align='center' '>".$sexe." ".$row['Nom']." ".$row['Prenom']."</td>
								    </tr>";
							}
								}
							echo "</table> <br>";
					 }mysqli_close($con);	
					 
	}
} 

?>
<br><br>
<form>
<input type="button" onclick="javascript:window.location.replace('etudiant_consulter_index.php')" class="return" value="Retourner">
</form>
<!--<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table >-->
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>