<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:../connexion_admin.php?erreur=true");
exit;
}

?>
<script>
function checkDelete()
{
  if(confirm('Vous êtes sûr de cette suppression ?'))return true;
  else return false;
}
</script>

<!DOCTYPE html>
<html>
<head>
<meta CHARSET="UTF-8">
<title>Professeur</title>
<link rel="stylesheet" type="text/css" link="./modif_style.css">
 <style>
     table {
                   width: 100%;
                   margin-top:1%;
                   border-width:5px;  
                   border-style:inset;
                   border-color:cornflowerblue;
              }
        th, td {
            padding: 5px;
  text-align: left;
    border-bottom: 1px solid #ddd;
    text-align:center;
}
        tr:hover {background-color :lightsteelblue}
        tr:nth-child(even) {background-color: #f2f2f2;}
         .button {
            background-color :dodgerblue;
            border: none;
            color: white;
            padding: 15px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size:20px;
            margin: 10px 1px;
            position:relative;
            top: 40px;
            left: 600px;
            right: 4px;
          
            font-family: 'Hind Vadodara',sans-serif;}
			   input[type="submit"] {
            background-color :dodgerblue;
            border: none;
            color: white;
            padding: 5px 5px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size:20px;
            position:relative;

          
            font-family: 'Hind Vadodara',sans-serif;}
			
			input[type=text]{

  width:20%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
       
	   .scolarite
{
    font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 46%;
    top: 33%;
    font-size:37px ;
    text-decoration: none;
    color: #1e1e1e;
}
      *
{
    margin:0;
    padding:0;
   
  }

	.menu img
{
    width: 50px;
    height: 60px;
    float: left;
    position: absolute;
    left: 41%;
    top: 18%;
}
input[type="button"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
.menu
{

	width: 100%;
	height: 16px;
	background-color: #fff;
	position: fixed;
	margin-top :-14%;
}
        .deco
{
   font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 91%;
    top: 52%;
    font-size:12px ;
    text-decoration: none;
    color: #1e1e1e;
  
}
		.menu img
{
    width: 50px;
    height: 60px;
    float: left;
    position: absolute;
    left: 41%;
    top: 18%;
}
.menu
{

	width: 100%;
	height: 76px;
	background-color: #fff;
	position: fixed;
	margin-top : -1%;
}    
	   .scolarite
{
    font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 46%;
    top: 33%;
    font-size:37px ;
    text-decoration: none;
    color: #1e1e1e;
}
        .deco
{
   font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 91%;
    top: 52%;
    font-size:12px ;
    text-decoration: none;
    color: #1e1e1e;
  
}
 
 </style>
</head>
<body>
	<header>
        <nav class="menu">
            <a href="../admin_principale.php" class="scolarite">Scolarité</a>
            <img src="../ump.png" alt="">
            <a href="../connexion_admin_deconnexion.php" class="deco">Se déconnecter</a>
        </nav>
    </header>
<br><br><br><br>

<?php
if(isset($_REQUEST['ajouter']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Ajout réussi</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
<?php
if(isset($_REQUEST['erreur']))
{
    echo "<h4 style='margin-left:5%;color:red;font-family:tahoma;'>Erreur: modification erronée</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
<?php
if(isset($_REQUEST['supprimer']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Suppression réussie</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
<?php
if(isset($_REQUEST['modifier']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Modification réussie</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
<?php
		 $con=mysqli_connect("127.0.0.1","root","","gestionds");
					 if($con)
					 {     
				       
							if($_POST['select']=='Nom')  $result=mysqli_query($con,"select * from professeur where lower(Nom) like '%".strtolower($_POST['recherche'])."%'");
							if($_POST['select']=='Prenom')  $result=mysqli_query($con,"select * from professeur where lower(Prenom) like '%".strtolower($_POST['recherche'])."%'");
							if($_POST['select']=='Discipline')  $result=mysqli_query($con,"select * from professeur where lower(Discipline) like '%".strtolower($_POST['recherche'])."%'");
							if($_POST['select']=='Email')  $result=mysqli_query($con,"select * from professeur where lower(Email) like '%".strtolower($_POST['recherche'])."%'");
							
							
							
                            if($result || mysqli_num_rows($result)>0)
							{						
									echo 
								"<table >
								
								<tr>
								<th >Nom</th>
								<th >Prenom</th>
								<th >Discipline</th>
								<th >Email</th>
								<th >Photo d'identité</th>
								<th >Activé</th>
								<th >Modifier</th>
								<th >Supprimer</th>
								</tr>
								";						
							while($row = mysqli_fetch_array($result))
							{	    $activate;
						            if($row['active']==1) $activate='Oui';
									if($row['active']==0) $activate='Non';
									$_SESSION['id_prof']=$row['id_prof'];
									$etat=true;		
									echo 
									"<tr>
								    <td align='center''>".$row['Nom']."</td>
									<td align='center' >".$row['Prenom']."</td>
									<td align='center' >".$row['Discipline']."</td>
									<td align='center' >".$row['Email']."</td>
									<td align='center' ><img style='border-radius:15px'src='../../professeur/database/".$row['Photo']."' height='100px'; width='80px';></td>
									<td align='center' >".$activate."</td>
									<td align='center' ><a href='modif_professeur/professeur_principale.php?id_prof=".$row['id_prof']."&nom=".$row['Nom']."&prenom=".$row['Prenom']."&Email=".$row['Email']."&discipline=".$row['Discipline']."&Photo=".$row['Photo']."&sexe=".$row['sexe']."'>Modifier</a></td>
									<td align='center'><a href='modif_professeur/supprimer_prof.php?id_prof=".$row['id_prof']."' onclick='return checkDelete();'>Supprimer</a></td>
								    </tr>";			
							}
							echo "</table> <br>";	
								
							}	
					 }mysqli_close($con);	
?>


<input type="button" onclick="javascript:window.location.replace('professeur_table.php')" value="Retourner">
</body>
</html>
