﻿<?php
session_start();
if(!isset($_SESSION['login']))
{
include 'connexion_admin_deconnexion.php';
header("location:connexion_index_erreur.php");
exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin page </title>
    <style>
      
        *
{
    margin:0;
    padding:0;
   
  }
        body{
              background: linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url(background.jpg);
              background-size:cover;
              background-position:bottom;   
              }


	.menu img
{
    width: 50px;
    height: 60px;
    float: left;
    position: absolute;
    left: 41%;
    top: 18%;
}
.menu
{

	width: 100%;
	height: 76px;
	background-color: #fff;
	position: fixed;
	margin-top : -1%;
}
.scolarite
{
    font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 46%;
    top: 33%;
    font-size:37px ;
    text-decoration: none;
    color: #1e1e1e;
}
.deco
{
   font-family: 'Hind Vadodara',sans-serif;
    letter-spacing: 2px;
    position: absolute;
    left: 91%;
    top: 52%;
    font-size:12px ;
    text-decoration: none;
    color: #1e1e1e;
  
}

    .a1{
        position:absolute;
        top:25%;
        left:51%;
    }
	.a2{
        position:absolute;
        top:26%;
        left:73%;
    }
     .a3{
        position:absolute;
        top:30%;
        left:33%;
    }
       .a5{
        position:absolute;
        top:64%;
        left:33%;
    }
    .a4{
        position:absolute;
        top:22%;
        left:10%;
    }
	.a6{
        position:absolute;
        top:64%;
        left:51%;
    }
	.a7{
        position:absolute;
        top:64%;
        left:73%;
    }
      .text {
        color: white;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
		 font-family: 'Hind Vadodara',sans-serif;
    }
  
       .overlay {
        position: absolute;
        top: 22px;
        bottom: 0;
        left: 20px;
        right: 0;
        height: 80%;
        width: 80%;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }
         .overlay2 {
        position: absolute;
        top: 46px;
        bottom: 0;
        left: 15px;
        right: 0;
        height: 64%;
        width: 85%;
        opacity: 0;
        transition: .5s ease;
        background-color:#93cfff91;
    }
      .overlay1 {
        position: absolute;
        top: 67px;
        bottom: 0;
        left: 26px;
        right: 0;
        height: 54%;
        width: 78%;
        opacity: 0;
        transition: .5s ease;
        background-color: #93cfff91;
    }
         .overlay3 {
        position: absolute;
        top: 0px;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #93cfff91;
    }
        
         .overlay5 {
        position: absolute;
        top: 13px;
        bottom: 0;
        left: 0;
        right: 0;
        height: 86%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #93cfff91;
    }


    .container:hover .overlay {
        opacity: 1;
		border-radius:15px;
    }
     .container:hover .overlay1 {
        opacity: 1;
		border-radius:15px;
    }
       .container:hover .overlay3 {
        opacity: 1;
		border-radius:15px;
    }
        .container:hover .overlay2 {
        opacity: 1;
		border-radius:15px;
    }
       .container:hover .overlay5 {
        opacity: 1;
		border-radius:15px;
    }
      


      .image {
        display: block;
        width: 100%;
        height: auto;
    }
  
input[type="image"]
{
	display:inline;
	margin-top:1%;
	margin-right:98%;
	margin-left:2%;
	width:50px;
	height:50px;
}
       .title{
        position:absolute;
        top:12%;
        left:5%;
		width:800px;
    }
    </style>
	<link rel="icon" href="ump.png" type="image/x-icon" />
</head>
<body>
    <header>
        <nav class="menu">
            <a href="../index.php" class="scolarite">Scolarité</a>
            <img src="ump.png" alt="">
            <a href="connexion_admin_deconnexion.php" class="deco">Se déconnecter</a>
        </nav>
    </header>
	<a class="title">
<?php
if(isset($_REQUEST['clean']))
{
    echo "<h4 style='margin-left:5%;color:green;font-family:tahoma;'>Mise à jour de base de données réussie</h4>";
	 unset($_REQUEST['clean']);
}
?><br>
<?php
if(isset($_REQUEST['erreur']))
{
    echo "<h4 style='margin-left:5%;color:red;font-family:tahoma;'>Mise à jour de base de données erronée</h4>";
	 unset($_REQUEST['erreur']);
}
?><br>
	</a>
    <a href="matiere/matiere_index1.php" class="a1">
        <div class="container">
            <img src="matiere.png" alt="matiere" class="image" style="width:200px;height:250px; ">
            <div class="overlay2">
                <div class="text">Matiere</div>
            </div>
        </div>
    </a>
    <a href="professeur/professeur_table.php" class="a2">
        <div class="container">
            <img src="professeur.png" alt="professeur" class="image" style="width:192px;height:229px ;">
            <div class="overlay">
                <div class="text">Professeur</div>
            </div>
        </div>
    </a>
    <a href="annee/annee_index.php" class="a3">
        <div class="container">
            <img src="annee.png" alt="annee" class="image" style="width:166px;height:181px; ">
            <div class="overlay3">
                <div class="text">Annee</div>
            </div>
        </div>
    </a>

    <a href="filiere/filiere_index.php" class="a4">
        <div class="container">
            <img src="filiere.png" alt="filiere" class="image" style="width:235px;height:291px;">
            <div class="overlay1">
                <div class="text">Filiere</div>
            </div>
        </div>

    </a>
 
    <a href="salle/salle_index.php" class="a5">
        <div class="container">
            <img src="class.png" alt="class" class="image" style="width:179px;height:204px;">
            <div class="overlay5">
                <div class="text">Salle</div>
            </div>
        </div>
    </a>
	
	<a href="exam/exam_index.php" class="a6">
        <div class="container">
            <img src="exam.png" alt="class" class="image" style="width:179px;height:204px;">
            <div class="overlay5">
                <div class="text">Examens</div>
            </div>
        </div>
    </a>
	
    <a href="clean_data.php" class="a7">
        <div class="container">
            <img src="clean.png" alt="class" class="image" style="width:179px;height:204px;">
            <div class="overlay5">
                <div class="text">Actualiser la base de données</div>
            </div>
        </div>
    </a>
	
</body>
</html>