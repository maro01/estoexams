<?php
session_start();
if(!isset($_SESSION['login']))
{
include '../connexion_deconnexion.php';
header("location:../connexion_index.php");
exit;
}
?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="icon" href="../ump.png" type="image/x-icon" />
<title>Ecole Superieure de Technologie</title>
<script src='inscription_myscript.js'></script>
<style>
div.copyright
{
	text-align:right;
	margin-top:125px;
}
h4
{
	font-family:tahoma;
}
table.media
{
	float:left;
	margin-top:105px;
	border-spacing:20px;
}
select.select
{
margin-right:88px;
}
table#boutons
{
    margin-left:auto;
	margin-right:auto;
}
input[type="submit"]
{
	color: rgb(91, 161, 212);
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
input[type="button"]
{
	color: rgb(91, 161, 212);
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
.postionate
{
	text-align:left;
	float:left;
}
pre
{
	font-size:12px;
}
table.inscription
{
	margin-left:auto;
	margin-right:auto;
}

img.logo
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}
input[type="submit"]
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
input[type="button"]
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
div > div.notice
{
	text-align:center;
	font-family:arial;
	color:rgb(227, 230, 228);
}
input[type="submit"]:hover
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
   background-color:rgb(57,121,218);
   color:white;
}
input[type="button"]:hover
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  background-color:rgb(57,121,218);
  color:white;
}
div.bleu
{
    background-color:rgb(165, 194, 237);
	width:100%;
	height:150px;
	color:white;
	font-family:tahoma;
	font-size:15px;
	text-align:center;
	padding:20px 0px;
	font-weight:bold;
}
span
{
	white-space: nowrap;
}
div.position
{
	margin-right:400px;
}
input[type=text]{

  width: 100%;	
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
</style>
</head>
<script>
function confirm_confid()
{
	if(	!( user_check() && password_check() ))
	{
	  alert('Merci de bien remplir votre formulaire');
	  event.preventDefault();
	  return false;
	}
	else return true;
}



function password_check()
{
 var regexp = /^\S*$/;
var passw = /^(?=.*[!@_*?])(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
var inputtxt=document.getElementById("password");
if(inputtxt.value.match(passw)  && regexp.test(inputtxt.value) ) 
{ let val=inputtxt.value;
if(val.length>=8 &&  val.length<=10 )
{
document.getElementById('password_label').style.color="rgb(255, 153, 0)";
document.getElementById('password_label').innerHTML="Faible";
}
if(val.length>10 &&  val.length<=15 )
{
document.getElementById('password_label').style.color="rgb(42,245,76)";
document.getElementById('password_label').innerHTML="Assez bien";
}
if(val.length>15 )
{
document.getElementById('password_label').style.color="rgb(0, 255, 0)";
document.getElementById('password_label').innerHTML="Parfait";
}
return true;
}
  if(document.getElementById('password').value=='')
  {
	  document.getElementById('password_label').innerHTML="";
  }
else
{ 
document.getElementById('password_label').style.color="red";
document.getElementById('password_label').innerHTML="Invalide";
return false;
}
}


function user_check()
{ var regexp = /^\S*$/;
if (/^(?=.*[a-zA-Z0-9]).{6,20}$/.test(document.getElementById("user").value) && regexp.test(document.getElementById("user").value))
  {
	document.getElementById('user_label').style.color="rgb(42,245,76)";
	document.getElementById('user_label').innerHTML="Valide";
    return true;
  }
    if(document.getElementById('user').value=='')
  {
	  document.getElementById('user_label').innerHTML="";
  }
  else
  {
    document.getElementById('user_label').style.color="red";
	document.getElementById('user_label').innerHTML="Invalide";
    return false;
  }
}
function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}

</script>
<body onload="myfunction1()">
<form name="formulaire" method="post" onsubmit="confirm_confid()" enctype="multipart/form-data">

<img  class="logo" src="../ump.png">
<h1 style='font-family:tahoma;color:rgb(91, 161, 212);'>Donnees confidentielles de compte</h1>
<div class="bleu">

<table class="inscription">
<tr>
<td>
<label for="nm" class="postionate">Nouveau nom d'utilisateur*</label>
</td>
<td>
<input type="text" size="25px" placeholder="Choisissez un nom d'utilisateur" name="user" id="user" onkeyup="user_check()"><br>
</td>
<td>
<span id="user_label"></span>
</td>
</tr>
<tr>
<td>
<label for="password" class="postionate">Nouveau mot de passe*</label>
</td>
<td>
<input type="text" size="25px" placeholder="Choisissez un mot de passe" name="password" id="password" onkeyup="password_check()"><br>
</td>
<td>
<span id="password_label"></span>
</td>
</tr>
<tr>
<td>
<label for="confirm" class="postionate">Confirmer le nouveau mot de passe*</label>
</td>
<td>
<input type="text" size="25px" placeholder="Confirmer votre motdepasse" name="confirm" id="confirm" ><br>
</td>
</tr>
</table>
</div>

<table class="inscription" >
<tr>
<td>
<input type="button" onclick='javascript:window.location.replace("professeur_principale.php?id_prof="+get("id_prof")+"&Email="+get("Email")+"&prenom="+get("prenom")+"&sexe="+get("sexe")+"&nom="+get("nom")+"&discipline="+get("discipline"))' value="Retourner">
</td>
<td>
<input type="button" onclick="javascript:window.location.replace('../professeur_principale.php')" value="Acceuil">
</td>
<td>
<input type="submit" name="submit" value="Continuer">
</td>
</tr>
</form>
</table>
<?php 

$_SESSION['id_prof']=$_REQUEST['id_prof'];
include 'confid_data.php';
 ?>

<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="../youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="../facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="../esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>