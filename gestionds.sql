-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2021 at 09:39 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gestionds`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrateur`
--

CREATE TABLE `administrateur` (
  `id_admin` int(20) NOT NULL,
  `motdepasse` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT 'NOT NULL',
  `nom_utilisateur` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT 'NOT NULL'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrateur`
--

INSERT INTO `administrateur` (`id_admin`, `motdepasse`, `nom_utilisateur`) VALUES
(0, '$2y$10$No5vIBwtW/NY0c.Xg9mGMeJgZZ61FVmsNafUtYnnq7ry2aL9ayNQi', '$2y$10$No5vIBwtW/NY0c.Xg9mGMeJgZZ61FVmsNafUtYnnq7ry2aL9ayNQi');

-- --------------------------------------------------------

--
-- Table structure for table `ajoutds`
--

CREATE TABLE `ajoutds` (
  `id_ds` int(20) NOT NULL,
  `filiere` varchar(100) NOT NULL,
  `matiere` varchar(40) NOT NULL,
  `libelle_annee` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `temps` time NOT NULL,
  `duree` time NOT NULL,
  `salle` varchar(5) NOT NULL,
  `id_prof` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ajoutds`
--

INSERT INTO `ajoutds` (`id_ds`, `filiere`, `matiere`, `libelle_annee`, `type`, `date`, `temps`, `duree`, `salle`, `id_prof`) VALUES
(66, 'DUT - Informatique et Gestion des Entreprises (IGE) ', 'Méthodes quantitatives', '2éme année', 'DS', '2020-04-15', '10:00:00', '01:20:00', 'D1', 243),
(67, 'LP - ISRI', 'Routage et commutation de base', 'LP', 'DS', '2020-06-12', '18:00:00', '01:20:00', 'D1', 243),
(72, 'DUT - Développeur des applications Informatiques (DAI)', 'Algorithmique et Programmation en C', '1ére année', 'DS', '2020-05-12', '16:20:00', '01:00:00', 'D3', 244),
(80, 'DUT - Développeur des applications Informatiques (DAI)', 'Structures de données en C++', '1ére année', 'DS', '2020-05-14', '14:00:00', '01:00:00', 'D1', 248),
(81, 'DUT - Génie Civil (GC)', 'Physique I', '1ére année', 'DS', '2020-05-16', '15:00:00', '00:30:00', 'D3', 248),
(82, 'LP - ISRI', 'Connecting networks', 'LP', 'DS', '2020-04-20', '14:00:00', '01:15:00', 'S69', 252),
(83, 'DUT - Gestion des Banques et Assurances (GBA) ', 'Langues et Préparation à la Vie Active', '1ére année', 'TP', '2020-06-11', '14:00:00', '01:00:00', 'D1', 252),
(84, 'DUT - Electronique et Informatique Industrielle (EII)', 'Informatique industrielle et API', '2éme année', 'DS', '2020-05-18', '13:00:00', '01:30:00', 'D4', 250),
(85, ' DUT - Finance, Comptabilité et Fiscalité (FCF)', 'Langues et Techniques d’expression et co', '1ére année', 'TP', '2020-05-12', '10:00:00', '01:15:00', 'D5', 250),
(86, 'DUT - Administrateur de Systèmes et Réseaux (ASR)', 'Technologies des serveurs', '2éme année', 'DS', '2020-04-25', '11:30:00', '01:00:00', 'D1', 251),
(87, 'DUT - Génie Electrique et Energies Renouvelables (GEER)', 'Mathématiques', '1ére année', 'DS', '2020-05-15', '15:00:00', '01:15:00', 'D1', 251),
(88, ' DUT - Technologie et Diagnostique Electronique Automobile (TDEA)', 'Langue/ Entreprenariat/Maintenance', '1ére année', 'DS', '2020-05-15', '16:30:00', '02:00:00', 'C1', 253),
(89, 'LP - MCOL', 'Informatique-SI Logistique', 'LP', 'DS', '2020-06-11', '15:00:00', '01:00:00', 'S74', 253);

-- --------------------------------------------------------

--
-- Table structure for table `annee`
--

CREATE TABLE `annee` (
  `id_annee` int(20) NOT NULL,
  `libelle_annee` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `annee`
--

INSERT INTO `annee` (`id_annee`, `libelle_annee`) VALUES
(1, '1ére année'),
(2, '2éme année'),
(3, 'LP');

-- --------------------------------------------------------

--
-- Table structure for table `filiere`
--

CREATE TABLE `filiere` (
  `id_filiere` int(20) NOT NULL,
  `libelle_filiere` text NOT NULL,
  `id_admin` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filiere`
--

INSERT INTO `filiere` (`id_filiere`, `libelle_filiere`, `id_admin`) VALUES
(1, 'DUT - Développeur des applications Informatiques (DAI)', 0),
(2, 'DUT - Administrateur de Systèmes et Réseaux (ASR)', 0),
(3, 'DUT - Electronique et Informatique Industrielle (EII)', 0),
(4, 'DUT - Génie Electrique et Energies Renouvelables (GEER)', 0),
(5, 'DUT - Génie Civil (GC)', 0),
(6, 'DUT - Mécatronique Industrielle (MC)', 0),
(7, ' DUT - Technologie et Diagnostique Electronique Automobile (TDEA)', 0),
(8, 'DUT - Gestion des Banques et Assurances (GBA) ', 0),
(9, ' DUT - Finance, Comptabilité et Fiscalité (FCF)', 0),
(10, 'DUT - Informatique et Gestion des Entreprises (IGE) ', 0),
(11, 'DUT - Gestion Logistique et Transport (GLT)', 0),
(12, 'DUT - Web Marketing (WM)', 0),
(13, 'DUT - Techniques de Vente et Service Client (TVSC)', 0),
(14, 'LP - ID', 0),
(15, 'LP - MI', 0),
(16, 'LP - ISRI', 0),
(17, 'LP - GE', 0),
(18, 'LP - MCOL', 0),
(19, 'LP - GCF', 0),
(20, ' LP - IGE', 0),
(21, 'DUT - Informatique de Gestion (IG)', 0),
(22, 'DUT - Génie Informatique Embarquée (GIE)', 0),
(23, 'DUT - Génie Industriel et Maintenance (GIM)', 0),
(24, 'Licences d éducation en Sciences Industrielles pour l’Ingénieur (Ledu SII)	', 0);

-- --------------------------------------------------------

--
-- Table structure for table `matiere`
--

CREATE TABLE `matiere` (
  `id_matiere` int(20) NOT NULL,
  `libelle_matiere` text NOT NULL,
  `id_filiere` int(20) NOT NULL,
  `id_annee` int(30) NOT NULL,
  `id_admin` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matiere`
--

INSERT INTO `matiere` (`id_matiere`, `libelle_matiere`, `id_filiere`, `id_annee`, `id_admin`) VALUES
(1, 'Algorithmique et Programmation en C', 1, 1, 0),
(2, 'Architecture des ordinateurs', 1, 1, 0),
(3, 'Mathématiques : Maths discrètes et algèbre linéaire', 1, 1, 0),
(4, 'Langues et Techniques d’expression et de communication en Français', 1, 1, 0),
(5, 'Langues et Techniques d’expression et de communication en Anglais', 1, 1, 0),
(6, 'Droit', 1, 1, 0),
(7, 'Structures de données en C++', 1, 1, 0),
(8, 'Système d’exploitation Linux', 1, 1, 0),
(9, 'Systèmes d’information et bases de données', 1, 1, 0),
(10, 'Réseaux informatiques et sécurité', 1, 1, 0),
(11, 'Ingénierie logicielle et .NET', 1, 2, 0),
(12, 'Programmation en Java et J2EE', 1, 2, 0),
(13, 'Mathématiques : modélisation math. Probabilité et Stat, Recherche opérationnelle', 1, 2, 0),
(14, 'Bases de données avancées', 1, 2, 0),
(15, 'Gestion et Ateliers de création d’entreprise (gestion de projets et développement de produits)', 1, 2, 0),
(16, 'Développement Web et XML', 1, 2, 0),
(17, 'Algorithmique et programmation en C', 2, 1, 0),
(18, 'Architecture des ordinateurs', 2, 1, 0),
(19, 'Mathématiques : Maths discrètes et algèbre linéaire', 2, 1, 0),
(20, 'Langues et Techniques d’expression et de communication en Français', 2, 1, 0),
(21, 'Langues et Techniques d’expression et de communication en Anglais', 2, 1, 0),
(22, 'Droit', 2, 1, 0),
(23, 'Structures de données en C++', 2, 1, 0),
(24, 'Systèmes d’exploitation Linux', 2, 1, 0),
(25, 'Systèmes d’information et bases de données', 2, 1, 0),
(26, 'Réseaux informatiques', 2, 1, 0),
(27, 'Technologies des serveurs', 2, 2, 0),
(28, 'Programmation Web et Java', 2, 2, 0),
(29, 'Mathématiques : Modélisation mathématique, Probabilités et Statistiques, Recherche opérationnelle', 2, 2, 0),
(30, 'Systèmes et Applications répartis', 2, 2, 0),
(31, 'Gestion et Ateliers de création d’entreprise', 2, 2, 0),
(32, 'Administration et sécurité des systèmes et réseaux', 2, 2, 0),
(39, 'Mathématiques', 3, 1, 0),
(40, 'Physique appliquée', 3, 1, 0),
(41, 'Informatique et informatique industrielle', 3, 1, 0),
(42, 'Langues et culture d’entreprises en Français', 3, 1, 0),
(43, 'Langues et culture d’entreprises en Anglais', 3, 1, 0),
(44, 'Fondements de l’électronique analogique', 3, 1, 0),
(45, 'Fondements de l’électrotechnique', 3, 1, 0),
(46, 'Etude d’équipements, travaux de réalisations et câblage', 3, 1, 0),
(47, 'Fonctions électroniques, modélisation et simulation', 3, 1, 0),
(48, 'Fonctions fondamentales de l’électronique', 3, 2, 0),
(49, 'Théorie et traitement du signal', 3, 2, 0),
(50, 'Informatique industrielle et API', 3, 2, 0),
(51, 'Automatique et électronique de puissance', 3, 2, 0),
(52, 'Communications analogiques et numériques', 3, 2, 0),
(53, 'Technique d’expression et Culture d’entreprise II', 3, 2, 0),
(54, 'Mathématiques', 4, 1, 0),
(55, 'Informatique & Informatique industrielle', 4, 1, 0),
(56, 'Physique', 4, 1, 0),
(57, 'formation humaine', 4, 1, 0),
(58, 'Electronique', 4, 1, 0),
(59, 'Electrotechnique', 4, 1, 0),
(60, 'Etudes d’équipement & travaux de réalisation', 4, 1, 0),
(61, 'Energies renouvelables, gisement solaire, éolien , biomasse et Efficacités Energétiques', 4, 1, 0),
(62, 'MATH-INFO', 4, 2, 0),
(63, 'Electrotechnique & système éolien', 4, 2, 0),
(64, 'Electronique de puissance, système PV et confort thermique', 4, 2, 0),
(65, 'Automatique et automate programmable', 4, 2, 0),
(66, 'Logique programmée : (microprocesseurs, microcontrôleurs)', 4, 2, 0),
(67, 'Communication professionnelle et Entrepreneuriat', 4, 2, 0),
(69, 'Introduction aux outils informatiques', 5, 1, 0),
(70, 'Géologie générale et matériaux de génie civil', 5, 1, 0),
(71, 'Bases Mathématique pour le technicien', 5, 1, 0),
(72, 'Physique I', 5, 1, 0),
(73, 'Anglais / gestion d’entreprises', 5, 2, 0),
(74, 'Informatique Appliquée', 5, 2, 0),
(75, 'Statistique, probabilité et analyse des données', 5, 2, 0),
(76, 'Physique II', 5, 2, 0),
(77, 'Anglais / système de transport', 5, 2, 0),
(78, 'Résistance des matériaux', 5, 2, 0),
(79, 'Topographie', 5, 2, 0),
(80, 'Hydraulique/hydrologie', 5, 2, 0),
(81, 'Béton armé / Routes', 5, 2, 0),
(82, 'Organisation / Sécurité', 5, 2, 0),
(83, 'Mathématique1, Informatique1 (Algorithmique, Architecture)', 6, 1, 0),
(84, 'Electricité Industrielle', 6, 1, 0),
(85, 'Mécanique des fluides, Thermodynamique & Machines Thermiques', 6, 1, 0),
(86, 'Langues, Environnement Entreprise', 6, 1, 0),
(87, 'Mathématiques Appliquées, Informatique Avancée', 6, 1, 0),
(88, 'Electronique Générale/Electronique Numérique', 6, 1, 0),
(89, 'Etudes des Systèmes Mécaniques, RDM', 6, 1, 0),
(90, 'Technologie des matériaux, Composants Mécaniques', 6, 1, 0),
(91, 'Régulation Industrielle, Commande Numérique', 6, 2, 0),
(92, 'Motorisations Electriques/Machines Electriques Spéciales, Maintenance Industrielle', 6, 2, 0),
(93, 'Contrôle Industriel, RLI, Supervision', 6, 2, 0),
(94, 'Conception Mécanique Intégrée (CAO 3D), Prototypage, API', 6, 2, 0),
(95, 'Technologie et Diagnostique Automobile', 6, 2, 0),
(96, 'Systèmes Embarqués, Electronique Programmée', 6, 2, 0),
(97, 'Procédure de Sécurité et Entretien Automobile', 7, 1, 0),
(98, 'Châssis – Carrosserie – Pneumatique et Suspension', 7, 1, 0),
(99, 'Systèmes de Freinage et Transmission', 7, 1, 0),
(100, 'Langue/ Entreprenariat/Maintenance', 7, 1, 0),
(101, 'Mathématiques et Informatique', 7, 1, 0),
(102, 'Electricité Automobile', 7, 1, 0),
(103, 'Diagnostique Electronique Automobile et Certifications', 7, 1, 0),
(104, 'Protections Electriques en BTA', 7, 1, 0),
(105, 'Motorisation Thermique– Réparation Moteur', 7, 2, 0),
(106, 'Electronique embarquée dans l’Automobile et OBD', 7, 2, 0),
(107, 'Carburation/Combustion/Pollution – Performance Moteur thermique', 7, 2, 0),
(108, 'Systèmes Mécatroniques asservis et Commande', 7, 2, 0),
(109, 'Numérique', 7, 2, 0),
(110, 'Plateforme Technologie Automobile – Etude Pratique', 7, 2, 0),
(111, 'Technologie des Véhicules Hybrides/Electriques', 7, 2, 0),
(112, 'Langues et TEC', 8, 1, 0),
(113, 'Environnement juridique et économique', 8, 1, 0),
(114, 'Informatique et mathématiques appliquées', 8, 1, 0),
(115, 'Techniques Comptables et Financières', 8, 1, 0),
(116, 'Langues et Préparation à la Vie Active', 8, 1, 0),
(117, 'Comptabilité, Droit bancaire et droit des assurances', 8, 1, 0),
(118, 'Informatique de Gestion et Statistiques', 8, 1, 0),
(119, 'Démarche Marketing', 8, 1, 0),
(120, 'Bases de données et outils de gestion', 8, 2, 0),
(121, 'Commercialisation des produits de Banque et d Assurance', 8, 2, 0),
(122, 'Outils de Gestion', 8, 2, 0),
(123, 'Comptabilisation des opérations de banque et d’assurance', 8, 2, 0),
(124, 'Finance et fiscalité', 8, 2, 0),
(125, 'Système bancaire et produits des assurances', 8, 2, 0),
(126, 'Langues et Techniques d’expression et communication', 9, 1, 0),
(127, 'Environnement juridique et économique', 9, 1, 0),
(128, 'Informatique et mathématiques appliquées', 9, 1, 0),
(129, 'Techniques Comptables et Financières', 9, 1, 0),
(130, 'Langues et Préparation à la Vie Active', 9, 1, 0),
(131, 'Comptabilité et Droit de l’entreprise', 9, 1, 0),
(132, 'Informatique de Gestion et Statistiques', 9, 1, 0),
(133, 'Démarche Marketing', 9, 1, 0),
(134, 'Bases de données et comptabilité informatisée', 9, 2, 0),
(135, 'Méthodes quantitatives', 9, 2, 0),
(136, 'Outils de Gestion', 9, 2, 0),
(137, 'Gestion comptable', 9, 2, 0),
(138, 'Finance et fiscalité', 9, 2, 0),
(139, 'Système bancaire et finance', 9, 2, 0),
(140, 'Internationale', 9, 2, 0),
(141, 'Langues, Techniques d’expression et communication', 10, 1, 0),
(142, 'Environnement juridique et Economique', 10, 1, 0),
(143, 'Informatique et mathématiques appliquées', 10, 1, 0),
(144, 'Techniques Comptables et Financières', 10, 1, 0),
(145, 'Langues et Préparation à la Vie Active', 10, 1, 0),
(146, 'Comptabilité et stratégie de l’entreprise', 10, 1, 0),
(147, 'Informatique de gestion et Statistique', 10, 1, 0),
(148, 'Démarche Marketing', 10, 1, 0),
(149, 'Bases de données et comptabilité informatisée', 10, 2, 0),
(150, 'Méthodes quantitatives', 10, 2, 0),
(151, 'Outils de Gestion', 10, 2, 0),
(152, 'Gestion financière et comptable', 10, 2, 0),
(153, 'Gestion de production, de la qualité et des projets', 10, 2, 0),
(154, 'Informatique de Gestion', 10, 2, 0),
(155, 'Langues et Communication I', 11, 1, 0),
(156, 'Environnement juridique et Economique I', 11, 1, 0),
(157, 'Marketing et Comptabilité', 11, 1, 0),
(158, 'Méthodes Quantitatives I', 11, 1, 0),
(159, 'Langues et Communication II', 11, 1, 0),
(160, 'Méthodes Quantitatives II', 11, 1, 0),
(161, 'Environnement juridique et Economique II', 11, 1, 0),
(162, 'Gestion logistique', 11, 1, 0),
(163, 'Économie des transports', 11, 2, 0),
(164, 'Techniques d’exploitation logistique', 11, 2, 0),
(165, 'Gestion des ressources humaines et gestion financière', 11, 2, 0),
(166, 'Informatique', 11, 2, 0),
(167, 'Chaîne logistique et transport', 11, 2, 0),
(168, 'Contrôle logistique', 11, 2, 0),
(169, 'Statistique Inférentielle / Logiciel R', 14, 3, 0),
(170, 'Techniques d enquête et de sondage', 14, 3, 0),
(171, 'Gestion de projets et Entreprenariat', 14, 3, 0),
(172, 'Développement WEB', 14, 3, 0),
(173, 'Bases de données', 14, 3, 0),
(174, 'Communication professionnelle et Business English', 14, 3, 0),
(175, 'Analyse de données', 14, 3, 0),
(176, 'Data Mining', 14, 3, 0),
(177, 'Gestion d’entrepôts de données et Construction de tableau de bords', 14, 3, 0),
(178, 'Communication professionnelle et business English', 16, 3, 0),
(179, 'Gestion de projets et entrepreneuriat', 16, 3, 0),
(180, 'Présentation des réseaux', 16, 3, 0),
(181, 'Gestion de l’adressage IP', 16, 3, 0),
(182, 'Routage et commutation de base', 16, 3, 0),
(183, 'Connecting networks', 16, 3, 0),
(184, 'Evolutivité de commutation', 16, 3, 0),
(185, 'Evolutivité de routage', 16, 3, 0),
(186, 'Implementing network security', 16, 3, 0),
(187, 'Management du système logistique global', 18, 3, 0),
(188, 'Logistique de la production et des stocks', 18, 3, 0),
(189, 'Informatique-SI Logistique', 18, 3, 0),
(190, 'Outils de gestion', 18, 3, 0),
(191, 'Droit et entrepreneuriat logistique', 18, 3, 0),
(192, 'Communication professionnelle', 18, 3, 0),
(193, 'Logistique de distribution et des transports', 18, 3, 0),
(194, 'Logistique et commerce international', 18, 3, 0),
(195, 'CG et audit des systèmes logistiques', 18, 3, 0),
(196, 'Contrôle de gestion', 19, 3, 0),
(197, 'Progiciels comptables et ERP', 19, 3, 0),
(198, 'Gestion fiscal et droit fiscal marocain', 19, 3, 0),
(199, 'Analyse et diagnostic financier', 19, 3, 0),
(200, 'Communication financière et Business communication', 19, 3, 0),
(201, 'La comptabilité de gestion', 19, 3, 0),
(202, 'Comptabilité approfondie', 19, 3, 0),
(203, 'Finance islamique', 19, 3, 0),
(204, 'La trésorerie et création d’entreprise', 19, 3, 0),
(205, 'Audit', 19, 3, 0),
(215, 'yyyyyyy', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `professeur`
--

CREATE TABLE `professeur` (
  `id_prof` int(5) NOT NULL,
  `Nom` varchar(25) NOT NULL,
  `Prenom` varchar(25) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `code` varchar(60) NOT NULL,
  `nom_utilisateur` char(60) NOT NULL,
  `motdepasse` char(60) NOT NULL,
  `Discipline` varchar(60) NOT NULL,
  `Photo` varchar(50) NOT NULL,
  `active` int(1) NOT NULL,
  `date_entree` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `professeur`
--

INSERT INTO `professeur` (`id_prof`, `Nom`, `Prenom`, `sexe`, `Email`, `code`, `nom_utilisateur`, `motdepasse`, `Discipline`, `Photo`, `active`, `date_entree`) VALUES
(248, 'Wayne', 'James', 'Homme', 'jameswayne@exemple.com', 'd1c38a09acc34845c6be3a127a5aacaf', '$2y$10$DlH2OIljLDpxkKRY9bofAOPPVEyK/yb64gcHQtRbtE3epCN/Fj2xW', '$2y$10$PVqOfhnRgyDxQISFJqEYr.uOq1N/uBT8cIHOghdlaZGrNitvdEr56', 'PROF DE MATHEMATIQUES', '2.JPG', 1, '2021-01-25'),
(250, 'Gomez', 'Sarah', 'Femme', 'sarahgomez@exemple.com', 'cc1aa436277138f61cda703991069eaf', '$2y$10$gNTuD/g3fRwrV5ccNAQj7eWC1OTVXupqIktv6B1i6nDKfOd43rSxG', '$2y$10$zgiJdA/Vg17.PP9T1aiQgeX1SDXy8R7vgNktaAIssjYUwXd98Ku5m', 'PROFESSEURE DE FRANCAIS', 'new.JPG', 1, '2020-04-17'),
(251, 'Smith', 'George', 'Homme', 'georgesmith@exemple.com', '00ac8ed3b4327bdd4ebbebcb2ba10a00', '$2y$10$5UgU74ux55e1Vu.Uj7TzUOqkHgfIvY50nrOE6dIAr5EuiDY/YzW.a', '$2y$10$euklUL29O5Qkj3jd3fXEfu/4XXkQXgJNNArD4RZ/B/ELouo.7BBwu', 'PROFESSEUR DE PHYSIQUE', '1.JPG', 1, '2021-01-25'),
(252, 'Derriche', 'Sebastien', 'Homme', 'sebastienderriche@exemple.com', '892c91e0a653ba19df81a90f89d99bcd', '$2y$10$y9IbZsavdCUBtlsHLctY1.WAlQNgNrBPAtZ24SdvkahpfLXe0ploq', '$2y$10$kycHOL8tVePC5/yJsvS/OuUztIdXiUJVYPAJz4r3JGc6.sOLnDxSu', 'PROFESSEUR D INFORMATIQUE', 'boy.jpg', 1, '2021-01-25'),
(253, 'Zielinska', 'Jenny', 'Homme', 'jennyzielinska@exemple.com', '7f1de29e6da19d22b51c68001e7e0e54', '$2y$10$Sm73F8xj/.anAjNbbHO98OUhvGnG9ZDh4Hk2T8aDyTBSd5AryjEDW', '$2y$10$ZCHl.1aPfx4UAKBbnJtmd.6OpR6LchL2LGveFuTsJyxN18ykZ/jri', 'PROFESSEURE DE COMPTABILITE', 'girl.jpg', 1, '2020-04-17');

-- --------------------------------------------------------

--
-- Table structure for table `salle`
--

CREATE TABLE `salle` (
  `id_salle` int(20) NOT NULL,
  `libelle_salle` text NOT NULL,
  `id_admin` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salle`
--

INSERT INTO `salle` (`id_salle`, `libelle_salle`, `id_admin`) VALUES
(1, 'D1', 0),
(2, 'D2', 0),
(3, 'D3', 0),
(4, 'D4', 0),
(5, 'D5', 0),
(6, 'C1', 0),
(7, 'C2', 0),
(8, 'C3', 0),
(9, 'S65', 0),
(10, 'S66', 0),
(11, 'S67', 0),
(12, 'S68', 0),
(13, 'S69', 0),
(14, 'S70', 0),
(15, 'S71', 0),
(16, 'S72', 0),
(17, 'S73', 0),
(18, 'S74', 0),
(19, 'S75', 0),
(20, 'S76', 0),
(21, 'S77', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `ajoutds`
--
ALTER TABLE `ajoutds`
  ADD PRIMARY KEY (`id_ds`),
  ADD KEY `id_prof` (`id_prof`);

--
-- Indexes for table `annee`
--
ALTER TABLE `annee`
  ADD PRIMARY KEY (`id_annee`);

--
-- Indexes for table `filiere`
--
ALTER TABLE `filiere`
  ADD PRIMARY KEY (`id_filiere`),
  ADD KEY `ID_Admin_FK3` (`id_admin`);

--
-- Indexes for table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id_matiere`),
  ADD KEY `ID_Admin_FK1` (`id_admin`),
  ADD KEY `annee_fk` (`id_annee`),
  ADD KEY `filiere_fk` (`id_filiere`);

--
-- Indexes for table `professeur`
--
ALTER TABLE `professeur`
  ADD PRIMARY KEY (`id_prof`) USING BTREE,
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `nom_utilisateur` (`nom_utilisateur`);

--
-- Indexes for table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id_salle`),
  ADD KEY `ID_Admin_FK2` (`id_admin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ajoutds`
--
ALTER TABLE `ajoutds`
  MODIFY `id_ds` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `annee`
--
ALTER TABLE `annee`
  MODIFY `id_annee` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `filiere`
--
ALTER TABLE `filiere`
  MODIFY `id_filiere` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id_matiere` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `professeur`
--
ALTER TABLE `professeur`
  MODIFY `id_prof` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=255;

--
-- AUTO_INCREMENT for table `salle`
--
ALTER TABLE `salle`
  MODIFY `id_salle` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `filiere`
--
ALTER TABLE `filiere`
  ADD CONSTRAINT `filiere_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `administrateur` (`id_admin`) ON UPDATE CASCADE;

--
-- Constraints for table `matiere`
--
ALTER TABLE `matiere`
  ADD CONSTRAINT `annee_fk` FOREIGN KEY (`id_annee`) REFERENCES `annee` (`id_annee`),
  ADD CONSTRAINT `filiere_fk` FOREIGN KEY (`id_filiere`) REFERENCES `filiere` (`id_filiere`),
  ADD CONSTRAINT `matiere_ibfk_1` FOREIGN KEY (`id_filiere`) REFERENCES `filiere` (`id_filiere`) ON UPDATE CASCADE;

--
-- Constraints for table `salle`
--
ALTER TABLE `salle`
  ADD CONSTRAINT `salle_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `administrateur` (`id_admin`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
