<?php
session_start();
if(!isset($_SESSION['login']))
{
include '../connexion_deconnexion.php';
header("location:../connexion_index.php");
exit;
}
?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="stylesheet" type="text/css" href="principale_prof_style.css" >
<link rel="icon" href="../ump.png" type="image/x-icon" />
<title>École Supérieure de Technologie</title>
</head>
<body>
<script>
function checkDelete()
{
  if(confirm('Vous êtes sûr de cette suppression ?'))return true;
  else return false;
}
function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}
function send_info1()
{
location.replace('modif_index.php?id_prof='+get('id_prof')+'&nom='+get('nom')+'&prenom='+get('prenom')+'&sexe='+get('sexe')+'&discipline='+get('discipline')+'&Email='+get('Email'));
}
function send_info2()
{
location.replace('confid_index.php?id_prof='+get('id_prof')+'&nom='+get('nom')+'&prenom='+get('prenom')+'&sexe='+get('sexe')+'&discipline='+get('discipline')+'&Email='+get('Email'));
}
</script>
<img class="centrale" src="../ump.png">
<a class="supprimer" onclick="return checkDelete()" size="25px" href="supprimer_prof.php">Supprimer votre compte</a>
<table class="choix_modif">
<tr>
<td>
<input type="image" class="choix_modif" src="info_perso.png" onclick="send_info1()" >
</td>
<td>
<input type="image" class="choix_modif" src="info_compte.png" onclick="send_info2()" >
</td>
</tr>
</table>
<table class="inscription" >
<tr>
<td>
<input type="button" onclick="javascript:window.location.replace('../professeur_principale.php')" value="Retourner">
</td>
</tr>
</table>
<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="../youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="../facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="../esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>