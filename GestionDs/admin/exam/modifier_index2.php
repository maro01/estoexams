<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="icon" href="../ump.png" type="image/x-icon" />
<link rel="stylesheet" type="text/css">
<style>

input[type="time"]
{
  margin-right:88px;
  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}

select.a
{
  margin-right:88px;
  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}


  div.copyright
{
	text-align:right;
	margin-top:190px;
}
h4
{
	
	font-family:tahoma;
}
img.centrale
{
display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}
table.media
{
	float:left;
	margin-top:170px;
	border-spacing:20px;
}
table#boutons
{
    margin-left:auto;
	margin-right:auto;
}
input[type="submit"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
input[type="button"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
.postionate
{
	text-align:left;
	float:left;
}
pre
{
	font-size:12px;
}
table.inscription
{
	margin-left:auto;
	margin-right:auto;
}

img.c
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}



input[type="submit"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
input[type="button"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
div > div.notice
{
	text-align:center;
	font-family:arial;
	color:rgb(227, 230, 228);
}


div.bleu
{
	width:100%;
	height:220px;
	background-color:rgb(165, 194, 237);
	color:white;
	font-family:tahoma;
	font-size:15px;
	text-align:center;
	padding:20px 0px;
	font-weight:bold;
}
div.position
{
	margin-right:400px;
}
p.notice
{
color:red;	
}
</style>
<link rel="icon" href="ump.png" type="image/x-icon" />
<script src="modif_myscript.js"></script>
<title>École Supérieure de Technologie</title>

<?php
session_start();

if(!empty($_POST['filiere']) && !empty($_POST['annee']) && !empty($_POST['type']) && !empty($_POST['date']) && 
!empty($_POST['temps']))
{
	
$_SESSION["filiere"]=$_POST['filiere'];
$_SESSION["annee"]=$_POST['annee'];
$_SESSION["type"]=$_POST['type'];
$_SESSION["date"]=$_POST['date'];
$_SESSION["temps"]=$_POST['temps'];

}
?>
</head>
<body onload="myfunction2()">
<img class="c" src="../ump.png">
<a id="return" onclick="return myfunction3()" >
<img style="  height:80px; width:80px;"src="return.png"> 
</a>
<form name="formulaire" method="post" enctype="multipart/form-data">

<div class="bleu">
<table class="inscription">
<tr>
<td>
<label for="matiere" class="postionate">Matière</label>
</td>
<td align="left">
<select name="matiere" id="matiere" class="a">
<optgroup name="Choisissez la matière en examen">
<?php
$con=mysqli_connect("localhost","id13248667_root","","gestionds");
if($con)
{

			$result=mysqli_query($con,"select * from matiere m , filiere f , annee a where m.id_filiere=f.id_filiere and m.id_annee=a.id_annee  
                              and f.libelle_filiere='".$_SESSION['filiere']."' and a.libelle_annee='".$_SESSION['annee']."'");
							  
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
			echo '<option value="'.$row["libelle_matiere"].'">'.$row["libelle_matiere"].'</option>';
	}
}
mysqli_close($con);

?>
</optgroup>
</select>
</td>
</tr>
<tr>
<td>
<label for="salle" class="postionate">Salle d'examen</label>
</td>
<td align="left">
<select name="salle" id="salle" class="a" >
<optgroup label="Choisissez la salle d'examen">
<?php
$con=mysqli_connect("localhost","id13248667_root","","gestionds");
if($con)
{
	$result=mysqli_query($con,'select * from salle');
	if($result)
	{
		while($row=mysqli_fetch_assoc($result))
			echo "<option value='".$row["libelle_salle"]."'>".$row["libelle_salle"]."</option>";
	}
}
mysqli_close($con);

?>
</optgroup>
</select>
</td>
</tr>
<tr>
<td>
<label for="duree" class="postionate">Duree d'examen en heures</label>
</td>
<td align="left">
<input type="time" style='width:168px' name="duree" id="duree" min="00:30" max="04:00" required><br>
</td>
</tr>
</table><br>
</div>
<table class="inscription" >
<tr>
<td>
<input type="button" name="acceuil" value="Accueil" onclick="history.replaceState(null,null,'exam_index.php');location.reload();">
</td>
<td>
<input type="submit" name="submit" value="Valider">
</td>
</form>

</tr>
</table>
<?php 
include 'modif_data.php'; ?>

<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>