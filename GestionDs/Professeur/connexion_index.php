<?php
session_start();
if(isset($_SESSION['login']) && isset($_SESSION['activated']))
{
if($_SESSION['activated'])
{
 echo "<script>location.replace('professeur_principale.php')</script>";
 exit;
}
if(!$_SESSION['activated'])
{
 echo "<script>location.replace('inscription_index2.php')</script>";
 exit;
}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Connection</title>
		<link rel="icon" href="ump.png" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="connexion_style.css" >
       <script src="connexion_myscript.js"></script>
    </head>
<body>
<div class = "c" >
<img id="image" class="centrale" src="ump.png" height="200px" width="300px" >
</div>
<form>
<input type="image"  class="return" src="return.png"  onclick="history.replaceState(null,null,'../index.php');"> 
</form>
<div class="login-box">
            <h1>Se connecter</h1>
        <form method="POST" action="connexion_checking.php" class="form-inline" >
            <div class="textbox">
                <label for="user"><img src="connexion_user.png" alt="Image ESTO"></label>
                <input type="text" name="user" id="user" placeholder="Nom d'utilisateur" >
            </div>
            <div class="textbox">
                <label for="password" ><img src="connexion_lock.png" alt="Motdepasse"></label>
				<input type="password" name="password" id="password" placeholder="Mot de passe">
            </div>
          <input class="btn" type="submit" name="valider" value="Se connecter" ><br><br>
          <a href="inscription_index1.php" style="color:#1e1e1e ;text-align: center;"> Vous n'avez pas de compte?</a>
        </form>
		<?php
if(isset($_REQUEST['erreur']))
{
    echo "<h4 style='color:red;font-family:tahoma;'>Nom d'utilisateur ou mot de passe sont incorrectes</h4>";
	 unset($_REQUEST['erreur']);
}
?>
<?php
if(isset($_REQUEST['valide']))
{
    echo "<h4 style='color:green;font-family:tahoma;'>Votre compte est activé avec réussite</h4>";
	 unset($_REQUEST['valide']);
}
?>
<?php
if(isset($_REQUEST['grave']))
{
    echo "<h4 style='color:red;font-family:tahoma;'>Erreur</h4>";
	 unset($_REQUEST['grave']);
}
?>
<?php
if(isset($_REQUEST['delete']))
{
    echo "<h4 style='color:red;font-family:tahoma;'>Votre compte est supprimé</h4>";
	 unset($_REQUEST['delete']);
}
?>
</div>
<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>