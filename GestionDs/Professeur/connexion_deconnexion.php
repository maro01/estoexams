<?php
session_start();
session_destroy();

if(!empty($_SESSION['id_prof']))
unset($_SESSION['id_prof']);

if(!empty($_SESSION['Nom']))
unset($_SESSION['Nom']);

if(!empty($_SESSION['Prenom']))
unset($_SESSION['Prenom']);

if(!empty($_SESSION['activated']))
unset($_SESSION['activated']);

if(!empty($_SESSION['Email']))
unset($_SESSION['Email']);

if(!empty($_SESSION['nom_utilisateur']))
unset($_SESSION['nom_utilisateur']);

if(!empty($_SESSION['motdepasse']))
unset($_SESSION['motdepasse']);

if(!empty($_SESSION['user']))
unset($_SESSION['user']);

if(!empty($_SESSION['password']))
unset($_SESSION['password']);

if(!empty($_SESSION['Discipline']))
unset($_SESSION['Discipline']);

if(!empty($_SESSION['access']))
unset($_SESSION['access']);

if(!empty($_SESSION['login']))
unset($_SESSION['login']);


if(isset($_REQUEST['valide'])) header('location:connexion_index.php?valide=true');
else if(isset($_REQUEST['erreur'])) header('location:connexion_index.php?erreur=true');
else if(isset($_REQUEST['grave'])) header('location:connexion_index.php?grave=true');
else if(isset($_REQUEST['delete'])) header('location:../professeur_index.php?delete=true');
else  header('location:connexion_index.php');

?>