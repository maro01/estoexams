﻿<?php session_start(); ?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">
<link rel="stylesheet" type="text/css" >
<link rel="icon" href="ump.png" type="image/x-icon" />
<style>
div.copyright
{
	text-align:right;
	margin-top:125px;
}
h4
{
	
	font-family:tahoma;
}
table.media
{
	float:left;
	margin-top:105px;
	border-spacing:20px;
}
input[type=text]{

  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}

select.select
{
  margin-right:88px;
  width: 100%;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}
table#boutons
{
    margin-left:auto;
	margin-right:auto;
}
input[type="submit"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
input[type="button"]
{
	margin: 10px 0px 0px 0px;
	font-family:tahoma;

}
.postionate
{
	text-align:left;
	float:left;
}
pre
{
	font-size:12px;
}
table.inscription
{
	margin-left:auto;
	margin-right:auto;

}

img.logo
{
  display: block;
  margin-left: auto;
  margin-right: auto;
  height:200px;
  width:300px;
}
input[type="submit"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
input[type="button"]
{
color: rgb(91, 161, 212);
  display: block;
  margin-left: auto;
  margin-right: auto;
  font-family:tahoma;
  font-size:15px;
  text-align:center;
  font-weight:bold;
  height:30px;
  width:100px;
}
div > div.notice
{
	text-align:center;
	font-family:arial;
	color:rgb(227, 230, 228);

}

div.bleu
{
    background-color:rgb(165, 194, 237);
	width:100%;
	height:450px;
	color:white;
	font-family:tahoma;
	font-size:15px;
	text-align:center;
	padding:20px 0px;
	font-weight:bold;
}
span
{
	white-space: nowrap;
}
div.position
{
	margin-right:400px;
}
</style>
<title>École Supérieure de Technologie</title>
<script src='inscription_myscript.js'></script>
</head>
<body>
<form name="formulaire" method="post" onsubmit="confirm_all()" enctype="multipart/form-data">

<img  class="logo" src="ump.png">

<div class="bleu">

<table class="inscription">
<tr>
<td>
<label for="nm" class="postionate">Nom</label>
</td>
<td>
<input type="text" size="20px" placeholder="Votre nom" name="nom" id="nm" onkeyup="nm_check()"><br>
</td>
<td>
<span id="nm_label"></span>
</td>
</tr>
<tr>
<td>
<label for="prnm" class="postionate">Prenom</label>
</td>
<td>
<input type="text" size="20px" placeholder="Votre prénom" name="prnm" id="prnm" onkeyup="prnm_check()"><br>
</td>
<td>
<span id="prnm_label"></span>
</td>
</tr>
<tr>
<td>


<label for="sexe" class="postionate">Sexe</label>
</td>
<td>
<select name="sexe" id="sexe" class="select" >
<optgroup label="Vous êtes ?">
<option value="Homme">Homme</option>
<option value="Femme">Femme</option>
</optgroup>
</select>




</td>
<td>
<span id="nm_label"></span>
</td>
</tr>
<tr>
<td>
<label for="email" class="postionate">E-mail</label>
</td>
<td>
<input type="text" size="20px" placeholder="Exemple : exemple@gmail.com" name="email" id="email" onkeyup="email_check()">
</td>
<td>
<span id="email_label"></span>
</td>
</tr>
<tr>
<td>
<label for="user" class="postionate">Nom d'utilisateur</label>
</td>
<td>
<input type="text" size="20px" placeholder="Exemple : user008" name="user" id="user" onkeyup="user_check()">
</td>
<td>
<span id="user_label"></span>
</td>
</tr>
<tr>
<td>
<label for="password" class="postionate">Mot de passe</label>
</td>
<td>
<input type="text" size="20px" placeholder="Exemple : User_008" name="password" id="password" onkeyup="password_check()">
</td>
<td>
<span id="password_label"></span>
</td>
</tr>
<tr>
<td>
<label for="confirm" class="postionate">Confirmer le mot de passe</label>
</td>
<td>
<input type="text" size="20px" placeholder="Confirmer votre motdepasse" name="confirm" id="confirm" onkeyup="confirm_pass()"><br>
</td>
</tr>
<tr>
<td>
<label for="dsplne" class="postionate">Discipline</label>
</td>
<td>
<input type="text" size="20px" placeholder="Votre Discipline" name="discipline" id="dsplne"><br>
</td>
</tr>
<tr>
<td><br>
<label for="pic" class="postionate">Photographie d'identité</label>
</td>
<td><br>
<input type="file" size="20px" placeholder="Votre Discipline" name="image" id="pic"><br>
</td>
<td>
<div class="notice">
<pre>
Scanner une photographie
 d'identité d'environ 26 mm
 de large et 32 mm de haut 
 de qualité 300 DPI.
 Le fichier doit être au format
 JPG ou PNG et ne doit pas
 dépasser une taille de 50 ko.
</pre>
</div>
</td>
</tr>
</table>

</div>
<table class="inscription" >
<tr>
<td>
<input type="button" onclick="javascript:window.location.replace('connexion_index.php')" value="Retourner">
</td>
<td>
<input type="submit" name="submit" value="Continuer">
</td>
</tr>
</table>
</form>
<?php 
include 'inscription_data.php'; 
?>

<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>