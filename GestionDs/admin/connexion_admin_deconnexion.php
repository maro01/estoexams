<?php
session_start();
session_destroy();
if(!empty($_SESSION['id_admin']))
unset($_SESSION['id_admin']);

if(!empty($_SESSION['nom_utilisateur']))
unset($_SESSION['nom_utilisateur']);

if(!empty($_SESSION['motdepasse']))
unset($_SESSION['motdepasse']);

if(!empty($_SESSION['access']))
unset($_SESSION['access']);

if(!empty($_SESSION['login']))
unset($_SESSION['login']);

header("location:connexion_admin.php");
?>