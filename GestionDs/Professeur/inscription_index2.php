﻿<?php
session_start();
if(isset($_SESSION['login']) && isset($_SESSION['activated']))
{
if($_SESSION['activated'])
{
 echo "<script>location.replace('professeur_principale.php')</script>";
 exit;
}
}
?>
<!DOCTYPE html>
<head>
<meta CHARSET="UTF-8">

<link rel="stylesheet" type="text/css" href="inscription_style2.css" >
<link rel="icon" href="ump.png" type="image/x-icon" />
<title>École Supérieure de Technologie</title>
</head>
<body>
<form id="form" method="post" action="inscription_index2_data.php" enctype="multipart/form-data">

<img  class="logo" src="ump.png">

<div class="bleu">
<p style="c">Vous recevez un code de vérification dans l'E-mail que vous avez saisissez , entrez ce code afin de finaliser votre inscription</p><br> <br> <br>
<table class="inscription">
<tr>
<td>
<label for="code" class="postionate">Code de vérification</label>
<br> <br>
<input type="text" size="50px" placeholder="Saisissez le code" name="code" id="code"style=" "><br>
</td>
</tr>
</table>

</div>
<table class="inscription" >
<tr>
<td>
<input type="submit" name="submit" value="Valider">
</form>
</td>
<td>
<form method="POST" action='verification_data.php'>
<input type="submit"  value="Envoyer">
</form>
</td>
<td>
<input  type="button" onclick="javascript:window.location.replace('inscription_index1.php')" value="Retourner">
</td>
<td>
<form method="POST" action="connexion_deconnexion.php" >
<input  type="submit" name="Deconnexion" value="Deconnexion" style="width:150px;">
</form>
</td>
</tr>
</table>
<?php
if(isset($_REQUEST['erreur']))
{
echo "<h4 style='font-family:tahoma;color:red;'>Code incorrecte</h4>";
unset($_REQUEST['erreur']);
}
?>
<?php
if(isset($_REQUEST['msg_erreur']))
{
echo "<h4 style='font-family:tahoma;color:red;'>Erreur: Message de verification non envoyé</h4>";
unset($_REQUEST['erreur_msg']);
}
?>
<?php
if(isset($_REQUEST['grave']))
{
echo "<h4 style='font-family:tahoma;color:red;'>Alerte: Un problème grave est trouvé , essayez de reconnecter à votre compte</h4>";
unset($_REQUEST['grave']);
}
?>
<?php
if(isset($_REQUEST['msg']))
{
echo "<h4 style='font-family:tahoma;color:green;'>Message est envoyé avec réussite</h4>";
unset($_REQUEST['msg']);
}
?>
<table class="media" align="left">
<tr>
<td><A href="https://www.youtube.com/channel/UCyFXZbYLiKOkthY26bCPn1Q"><img  src="youtube.jpg" height="15px" width="30px"></A></td>
<td><A href="https://www.facebook.com/UniversityOUJDA/"><img  src="facebook.jpg" height="15px" width="30px"></A></td>
<td><A href="http://esto.ump.ma/"><img  src="esto.jpg" height="15px" width="30px"></A></td>
</tr>
</table>
<div class="copyright">Copyright © 2020 Université Mohammed Premier Oujda</div>
</body>
</html>