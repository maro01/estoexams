function email_check()
{ var regexp = /^\S*$/;
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.getElementById('email').value) && regexp.test(document.getElementById('email').value) )
  {
	document.getElementById('email_label').style.color="rgb(42,245,76)";
	document.getElementById('email_label').innerHTML="Valide";
    return true;
  }
  if(document.getElementById('email').value=='')
  {
	  document.getElementById('email_label').innerHTML="";
  }
  else
  {
    document.getElementById('email_label').style.color="red";
	document.getElementById('email_label').innerHTML="Invalide";
    return false;
  }
}

function user_check()
{ var regexp = /^\S*$/;
if (/^(?=.*[a-zA-Z0-9]).{6,20}$/.test(document.getElementById("user").value) && regexp.test(document.getElementById("user").value))
  {
	document.getElementById('user_label').style.color="rgb(42,245,76)";
	document.getElementById('user_label').innerHTML="Valide";
    return true;
  }
    if(document.getElementById('user').value=='')
  {
	  document.getElementById('user_label').innerHTML="";
  }
  else
  {
    document.getElementById('user_label').style.color="red";
	document.getElementById('user_label').innerHTML="Invalide";
    return false;
  }
}

function nm_check()
{ var regexp = /^\S*$/;
if (/^(?=.{2,50}$)[a-z]+(?:['_.\s][a-z]+)*$/i.test(document.getElementById("nm").value) && regexp.test(document.getElementById("nm").value))
  {
	document.getElementById('nm_label').style.color="rgb(42,245,76)";
	document.getElementById('nm_label').innerHTML="Valide";
    return true;
  }
    if(document.getElementById('nm').value=='')
  {
	  document.getElementById('nm_label').innerHTML="";
  }
  else
  {
    document.getElementById('nm_label').style.color="red";
	document.getElementById('nm_label').innerHTML="Invalide";
    return false;
  }
}

function prnm_check()
{ var regexp = /^\S*$/;
if (/^(?=.{2,50}$)[a-z]+(?:['_.\s][a-z]+)*$/i.test(document.getElementById("prnm").value) && regexp.test(document.getElementById("prnm").value))
  {
	document.getElementById('prnm_label').style.color="rgb(42,245,76)";
	document.getElementById('prnm_label').innerHTML="Valide";
    return true;
  }
    if(document.getElementById('prnm').value=='')
  {
	  document.getElementById('prnm_label').innerHTML="";
  }
  else
  {
    document.getElementById('prnm_label').style.color="red";
	document.getElementById('prnm_label').innerHTML="Invalide";
    return false;
  }
}

function password_check()
{ var regexp = /^\S*$/;
var passw = /^(?=.*[!@_*?])(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
var inputtxt=document.getElementById("password");
if(inputtxt.value.match(passw)  && regexp.test(inputtxt.value) ) 
{ let val=inputtxt.value;
if(val.length>=8 &&  val.length<=10 )
{
document.getElementById('password_label').style.color="rgb(255, 153, 0)";
document.getElementById('password_label').innerHTML="Faible";
}
if(val.length>10 &&  val.length<=15 )
{
document.getElementById('password_label').style.color="rgb(42,245,76)";
document.getElementById('password_label').innerHTML="Assez bien";
}
if(val.length>15 )
{
document.getElementById('password_label').style.color="rgb(0, 255, 0)";
document.getElementById('password_label').innerHTML="Parfait";
}
return true;
}
  if(document.getElementById('password').value=='')
  {
	  document.getElementById('password_label').innerHTML="";
  }
else
{ 
document.getElementById('password_label').style.color="red";
document.getElementById('password_label').innerHTML="Invalide";
return false;
}
}

function confirm_pass()
{ 
if(document.getElementById('confirm').value==document.getElementById('password').value) return true;
else return false;
}

function confirm_all()
{
	if(	!(email_check() && user_check() && password_check() && confirm_pass() && nm_check() && prnm_check()) )
	{
	  alert('Merci de bien remplir votre formulaire');
	  event.preventDefault();
	  return false;
	}
	else return true;
	
}